+++
title = "Join the Hyperworld community!"
description = "Join the Hyperworld community!"

weight = 0
+++

Hyperworld is an open-source, community-driven project! Join us in making the game the best it can be.

## News

* [This Week In Hyperworld — Weekly dev blog](https://hyperworld-official.gitlab.io/devblogs/)
* [Mastodon — Keep up to date with the project](https://floss.social/@hyperworld)
* [Twitter — Keep up to date with the project](https://twitter.com/Hyperworldproject/)

## Community

* [Reddit — Discuss Hyperworld](https://www.reddit.com/r/Hyperworld/)
* [Youtube — Watch videos about development news](https://youtube.com/channel/UCmRjlnKnSRRihWPPNasl_Qw) | [(RSS)](https://www.youtube.com/feeds/videos.xml?channel_id=UCmRjlnKnSRRihWPPNasl_Qw) | [(Invidious)](https://yewtu.be/channel/UCmRjlnKnSRRihWPPNasl_Qw) | [(Invidious - RSS)](https://yewtu.be/feed/channel/UCmRjlnKnSRRihWPPNasl_Qw)
* [Twitch — Developer Channel](https://www.twitch.tv/Hyperworld_dev/videos) | [(Live Channels)](https://www.twitch.tv/directory/game/Hyperworld)

## Wiki & Information

* [Hyperworld Wiki — Contribute to or read articles about playing the game](https://wiki.hyperealm.net/)
* [The Book — A technical manual for players and developers](https://book.hyperealm.net/)

## Source Code

* [Gitlab — Read and contribute to the project's code](https://gitlab.com/hyperworld/hyperworld)
* [GitHub (MIRROR) — Read the project's code](https://github.com/hyperworld/hyperworld)

*It has unfortunately come to our attention that Hyperworld's assets, code, and imagery have, on occasion, been used to
promote malicious scams such as 'pump-and-dump' schemes, often involving cryptocurrency or NFTs. The core development
team is clear: Hyperworld is not, and will never be, a for-profit project, nor do we wish to be associated with regressive,
socially harmful technology like cryptocurrency.*
