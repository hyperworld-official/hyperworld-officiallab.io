+++
title = "Other download options"
description = "Other download options"

aliases = ["stable"]

weight = 0
+++

**Please note that currently the official server hosted at** `server.hyperealm.net`
**runs the nightly version so you have to run your own server or play singleplayer**
**if you're not using Hyperlauncher or the latest nightly.**

If you want to play multiplayer, you should [download Hyperlauncher](@/download.md) instead.

To play the game, extract all files and run `hyperworld-portal`.

## Nightly

You can download nightly here directly without the launcher but note that for any new update you will have to download the nightly here again.

* [Windows x64](https://download.hyperealm.net/latest/windows/x86_64/nightly)
* [Linux x64](https://download.hyperealm.net/latest/linux/x86_64/nightly)
* [Mac x64](https://download.hyperealm.net/latest/macos/x86_64/nightly)

#### Flatpak

This package is usually out of sync with the main server, but is still updated rather regularly.

`flatpak install flathub net.hyperworld.hyperworld`

## Linux packages (Latest stable)

#### Arch

[AUR latest binary release](https://aur.archlinux.org/packages/hyperworld-bin/
): `yay -Sy hyperworld-bin`

[AUR latest release](https://aur.archlinux.org/packages/hyperworld/
): `yay -Sy hyperworld`

## Hyperlauncher Package listing (most are unofficial)

[![Packaging status](https://repology.org/badge/vertical-allrepos/hyperlauncher.svg?columns=3)](https://repology.org/project/hyperlauncher/versions)

## Older versions of Hyperworld

### 0.1.0

* [Windows x64](https://gitlab.com/hyperworld1/hyperworld/-/jobs/artifacts/v0.13.0/download?job=windows-x86_64)
* [Linux x64](https://gitlab.com/hyperworld1/hyperworld/-/jobs/artifacts/v0.13.0/download?job=linux-x86_64)
* [Linux aarch64](https://gitlab.com/hyperworld1/hyperworld/-/jobs/artifacts/v0.13.0/download?job=linux-aarch64)
* [macOS x64](https://gitlab.com/hyperworld1/hyperworld/-/jobs/artifacts/v0.13.0/download?job=macos-x86_64)
* [macOS aarch64](https://gitlab.com/hyperworld1/hyperworld/-/jobs/artifacts/v0.13.0/download?job=macos-aarch64)
