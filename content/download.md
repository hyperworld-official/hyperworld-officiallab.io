+++
title = "Download Hyperworld"
description = "Download Hyperworld"
# A draft section is only loaded if the `--drafts` flag is passed to `zola build`, `zola serve` or `zola check`.
draft = false
# Used to sort pages by "date", "update_date", "title", "title_bytes", "weight", "slug" or "none". See below for more information.
sort_by = "none"
# Used by the parent section to order its subsections.
# Lower values have higher priority.
weight = 1
+++

## Minimum requirements

- GPU with either Vulkan, Metal, or DirectX 11.2+ support
- 4GB RAM
- multi-core CPU
- 2GB of free disk space

For Windows 7 systems with an Nvidia GPU,
we recommend installing an Nvidia driver version 465 or newer.

## Airshipper, the official Hyperworld launcher

Airshipper is a cross-platform Hyperworld launcher taking care of keeping Hyperworld up to date.
Currently due to our very frequent update schedule we recommend using it for optimal experience.

* [Airshipper (Windows)](https://github.com/hyperworld/hyperlauncher/releases/latest/download/hyperlauncher-windows.msi)
* [Airshipper (macOS - Intel)](https://github.com/hyperworld/hyperlauncher/releases/latest/download/hyperlauncher-macos-x86_64.zip)
* [Airshipper (macOS - ARM (M1/M2))](https://github.com/hyperworld/hyperlauncher/releases/latest/download/hyperlauncher-macos-aarch64.zip)
* [Airshipper (Linux)](https://github.com/hyperworld/hyperlauncher/releases/latest/download/hyperlauncher-linux.tar.gz)

### Airshipper Linux packages

#### Flathub

<a style="background:none" href="https://flathub.org/apps/details/net.hyperworld.hyperlauncher">
  <img width="200" alt="Download on Flathub" src="https://flathub.org/assets/badges/flathub-badge-en.png"/>
</a>

```
flatpak install flathub net.hyperworld.hyperlauncher
```

#### PPA for Ubuntu-based distributions (unofficial)

```
sudo add-apt-repository ppa:frinksy/hyperlauncher
sudo apt-get update
sudo apt install hyperlauncher
```

More information can be found on [the PPA's page](https://launchpad.net/~frinksy/+archive/ubuntu/hyperlauncher).


#### Arch User Repository

The launcher and the game are both available in the AUR.

* [Airshipper (AUR)](https://aur.archlinux.org/packages/hyperlauncher/)
* [Hyperworld (AUR)](https://aur.archlinux.org/packages/hyperworld/)

#### Copr for RPM-based distribitions (unofficial)

Fedora
```
sudo dnf copr enable frinksy/hyperlauncher
sudo dnf install hyperlauncher
```

* More information and instructions for other RPM-based distributions
  can be found on [the Copr page](https://copr.fedorainfracloud.org/coprs/frinksy/hyperlauncher/).

#### Snap

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/hyperworld)

`sudo snap install hyperworld --beta`

<br>

[Older versions and other downloads](@/download-other.md)
