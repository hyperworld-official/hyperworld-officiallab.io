+++
title = "Two Weeks in Hyperworld 207"
description = "Visual Guides"

date = 2023-03-13
weight = 0
slug = "devblog-207"

[extra]
banner = "https://cdn.discordapp.com/attachments/597826574095613962/1084608867998109776/screenshot_1678301670059.png"

[taxonomies]
tags = ["devblog"]
+++

These weeks, we see work on creating visual guides for people new to Hyperworld, progress on rtsim2, and a poll for a skill tree overhaul.

\- Sam, TWiV Contributor

# Contributor Work

Thanks to last two weeks' contributors, @##.

These changes include culling of underground terrain for rendering, more spots such as campsites, the ability to lock camera zoom, and optimizations related to buffs and auras.

Ongoing (unmerged) work is happening on sword rework, durability (death penalty), adlet caves, more food types, unlockable recipes, quests, arches, and many more.

{{img(src="https://cdn.discordapp.com/attachments/#")}}

# New Developers

We recently added two new core developers. Welcome @Isse and @aweinstock to the core dev team.

# Work on AI by @#

This week I've been doing some work on AI, in hopes of getting a version that's not a regression from master merged soon.

# Recent Player Growth

We recently saw a large influx of players that have pushed both the official server and community servers to the limits. Because of this, there has been some recent focus on optimizations as well as a decrease to the max player limit on the official server.

# Assorted game impressions

{{img(src="https://cdn.discordapp.com/attachments/#")}}

{{
    img(src="https://cdn.discordapp.com/attachments/#",
    caption="Isn't this magical? See you next week!")
}}

{{ support() }}
